import React, { useEffect } from 'react';
import { Provider } from 'react-redux';
import AppNavigator from './app/navigation';
import Store from './app/store';

import PushNotificationIOS from '@react-native-community/push-notification-ios';
import { initAmber } from './app/services/amberdata';
import { Platform } from 'react-native';

const App = () => {

    useEffect(() => {
        // this could be somewhere else but wanted to use the useEffect hook somewhere :)
        initAmber().then(() => { }).catch((e) => { console.log(e) }) // ignore success, log any error
        if (Platform.OS === 'ios') PushNotificationIOS.requestPermissions()
    }, [])

    return (
        <Provider store={Store}>
            <AppNavigator />
        </Provider>
    )
};

export default App;
