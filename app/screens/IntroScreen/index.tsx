import React, { useEffect, useState } from 'react'
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    ActivityIndicator
} from 'react-native'
import { useSelector, useDispatch } from 'react-redux'
import { MaterialColors, Typography } from '../../theme'
import { initAmber, verifyRandomPendingHash } from '../../services/amberdata'
import styles from './styles'
import { loadContactAction, verifyHashAction } from '../../store/actions'
import { initialState } from '../../store/reducers'
import PushNotification from 'react-native-push-notification';
import { showNotification } from '../../utils'
import { useNavigation } from 'react-navigation-hooks'
const IntroScreen = () => {
    const [hash, setHash] = useState();

    const { navigate } = useNavigation();

    const state = useSelector((state: { appData: initialState }) => state)
    console.log('state')
    console.log(state)
    console.log('state')

    const dispatch = useDispatch()

    const onVerify = () => {
        if (hash) {
            dispatch(verifyHashAction(hash))
        }
    }
    return (
        <View style={styles.container}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <View style={styles.headerContainer}>
                    <Text style={styles.heading}>Insert transaction hash to verify</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <TextInput value={state.appData.currentTransaction || undefined} editable={!state.appData.currentTransaction} onChangeText={setHash} placeholder={'hash here'} style={styles.input} />
                        <TouchableOpacity disabled={!!state.appData.currentTransaction} onPress={onVerify} style={{ marginLeft: '5%', width: '30%' }}>
                            <View style={[styles.buttonWrapper, { backgroundColor: MaterialColors.green[400], marginTop: 0 }]}>
                                <Text style={styles.buttonText}>{state.appData.currentTransaction ? 'Verifying..' : 'Verify!'}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>

            {state.appData.currentTransaction && (
                <View style={{ flexDirection: 'row', alignItems: 'center', padding: 12, paddingBottom: 4 }}>
                    <ActivityIndicator />
                </View>
            )}

            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <TouchableOpacity disabled={!!state.appData.currentTransaction} onPress={verifyRandomPendingHash}>
                    <View style={styles.buttonWrapper}>
                        <Text style={styles.buttonText}>Verify random pending transaction</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 20 }}>
                <TouchableOpacity onPress={() => dispatch(loadContactAction())}>
                    <View style={[styles.buttonWrapper, { backgroundColor: MaterialColors.green[400] }]}>
                        <Text style={styles.buttonText}>Load Contact</Text>
                    </View>
                </TouchableOpacity>
            </View>

            {state.appData.loadedContact && renderContact(state.appData.loadedContact)}

            <View style={{ flexDirection: 'row', alignItems: 'center', position: 'absolute', bottom: 20 }}>
                <TouchableOpacity onPress={() => navigate('TransactionsScreen')}>
                    <View style={[styles.buttonWrapper, { backgroundColor: MaterialColors.green[400] }]}>
                        <Text style={styles.buttonText}> Transaction List </Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const renderContact = (contact) => {
    return (
        <View style={{ flexDirection: 'column', alignItems: 'center', marginTop: 0 }}>
            <Text style={[styles.item, { color: MaterialColors.blue[400] }]}>Contact Loaded:</Text>
            {contact.name && <Text style={styles.item}>{contact.name}</Text>}
            {contact.phone && <Text style={styles.item}>{contact.phone}</Text>}
            {contact.email && <Text style={styles.item}>{contact.email}</Text>}
        </View>
    )
}

export default IntroScreen
