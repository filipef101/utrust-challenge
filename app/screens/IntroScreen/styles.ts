import {
    StyleSheet,
} from 'react-native'
import { MaterialColors, Typography } from '../../theme'

export default StyleSheet.create({ // TODO styles should go to a separate file and refractor styles in jsx
    input: {
        width: '60%',
        borderWidth: 1,
        padding: 4,
        borderColor: 'black'
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    headerContainer: {
        marginLeft: 10
    },
    logo: {
        height: 64,
        width: 64
    },
    heading: {
        ...Typography.Body.light,
        fontSize: 24
    },
    body: {
        ...Typography.Body.light,
        color: MaterialColors.grey[500],
        fontSize: 16
    },
    item: {
        ...Typography.Body.light,
        marginTop: 10,
        fontSize: 16
    },
    buttonWrapper: {
        // marginTop: 70,
        // marginLeft: 20,
        // marginRight: 20,
        // height: 20,
        marginTop: 10,

        flexDirection: 'column',
        backgroundColor: '#00CCFF',
        borderRadius: 4
    },
    buttonText: {
        ...Typography.Body.light,
        fontSize: 16,
        justifyContent: 'center',
        alignSelf: 'center',
        padding: 3,
        // marginTop: 3,
        // height: 15,
        // marginBottom: 3,
        // marginHorizontal: 5,
        elevation: 1,
        color: '#FFFFFF'
    }
})