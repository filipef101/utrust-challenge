import React from 'react'
import {
    Text,
    View,
    TouchableOpacity,
    FlatList,
} from 'react-native'
import { useSelector } from 'react-redux'
import { MaterialColors } from '../../theme'
import styles from './styles'
import { initialState } from '../../store/reducers'
import { useNavigation } from 'react-navigation-hooks'

function Item({ item }) {
    return (
        <View style={styles.item}>
            <Text style={styles.body}>{item.hash}</Text>
            <Text style={[styles.body, { color: 'black' }]}>{item.title + ' ' + item.verified ? 'Verified' : 'Not verified'}</Text>
        </View>
    );
}
const transactionsToData = (transactions) => {
    const data = [] as { hash: string, verified: boolean }[]
    for (const key in transactions) {
        if (transactions.hasOwnProperty(key)) {
            const element = transactions[key];
            data.push({ hash: key, verified: element })
        }
    }
    return data
}
const TransactionsScreen = () => {

    const transactions = useSelector((state: { appData: initialState }) => state).appData.transactions

    const data = transactionsToData(transactions)
    return (
        <View style={styles.container}>
            <FlatList
                data={data}
                style={{ width: '100%' }}
                renderItem={({ item }) => <Item item={item} />}
                keyExtractor={item => item.hash}
                ListFooterComponent={renderFooter}
                ItemSeparatorComponent={() => <View style={{ width: '100%', height: 1, backgroundColor: 'black' }} />}
            />
        </View>

    )
}
const renderFooter = () => {
    const { navigate, goBack } = useNavigation();
    return (
        <View style={{ flexDirection: 'row', alignItems: 'center', alignSelf: 'center', margin: 20 }}>
            <TouchableOpacity onPress={() => goBack()}>
                <View style={[styles.buttonWrapper, { backgroundColor: MaterialColors.green[400] }]}>
                    <Text style={styles.buttonText}>Go back</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}

export default TransactionsScreen
