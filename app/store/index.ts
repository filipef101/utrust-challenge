import { applyMiddleware, combineReducers, createStore } from 'redux';
import thunk from 'redux-thunk';
import Reducer from './reducers';

// could have done a reducer for the contacts and another for the transactions but don't think is needed
const Reducers = {
    appData: Reducer
};

const Store = createStore(combineReducers(Reducers), applyMiddleware(thunk));

export default Store;
