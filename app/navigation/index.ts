import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { IntroScreen, TransactionsScreen } from '../screens';



// let localNotification = PushNotificationsIOS.localNotification();
const StackNavigator = createStackNavigator({
    IntroScreen: {
        screen: IntroScreen
    },
    TransactionsScreen: {
        screen: TransactionsScreen
    }
}, { headerMode: 'none' });

const AppNavigator = createAppContainer(StackNavigator);

export default AppNavigator;
